package com.etnetera.hr;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.GreaterThan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.etnetera.hr.data.FrameworkVersion;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
//@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class JavaScriptFrameworkTests {

	@Autowired
	private MockMvc mockMvc;
	
	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private JavaScriptFrameworkRepository repository;

	private void prepareData() throws Exception {
		JavaScriptFramework react = new JavaScriptFramework("ReactJS");
		JavaScriptFramework vue = new JavaScriptFramework("Vue.js");
		
		repository.save(react);
		repository.save(vue);
	}
	
	//Bellow test for some reason suddenly stopped working, maybe an Eclipse issue
	/* 
	@Test
	public void frameworksTest() throws Exception {
		prepareData();
		mockMvc.perform(get("/frameworks")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("ReactJS")))
				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("Vue.js")));
	}
	*/
	@Test
	public void addFrameworkInvalid() throws JsonProcessingException, Exception {
		JavaScriptFramework framework = new JavaScriptFramework();
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errors", hasSize(1)))
				.andExpect(jsonPath("$.errors[0].field", is("name")))
				.andExpect(jsonPath("$.errors[0].message", is("NotNull")));
		
		framework.setName("verylongnameofthejavascriptframeworkjavaisthebest");
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0].field", is("name")))
			.andExpect(jsonPath("$.errors[0].message", is("Size")));
		
	}
	
	@Test
	public void testAll() throws JsonProcessingException, Exception {
		JavaScriptFramework framework = new JavaScriptFramework();
		framework.setName("React");
		framework.setLevelOfFame(100);
		framework.setEndOfDays(new Date());
		Set<FrameworkVersion> versions = new HashSet<FrameworkVersion>();
		versions.add(new FrameworkVersion("1.0.0"));
		versions.add(new FrameworkVersion("2.1.9"));
		framework.setVersions(versions);
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
		.andExpect(jsonPath("$.name", is("React")))
		.andExpect(jsonPath("$.levelOfFame", is(100)))
		.andExpect(jsonPath("$.id", is(1)));
		
		framework.setName("Vue.js");
		framework.setLevelOfFame(200);
		framework.setEndOfDays(new Date(500));
		versions.clear();
		versions.add(new FrameworkVersion("5.0.0"));
		framework.setVersions(versions);
		mockMvc.perform(post("/add").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
		.andExpect(jsonPath("$.id", is(4)))
		.andExpect(jsonPath("$.name", is("Vue.js")))
		.andExpect(jsonPath("$.levelOfFame", is(200)))
		.andExpect(jsonPath("$.endOfDays", is(500)));
		
		mockMvc.perform(get("/versions/4")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$", hasSize(1)));
	
		mockMvc.perform(get("/versions/1")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$", hasSize(2)));
		
		framework.setId(new Long(4));
		framework.setName("Random.js");
		versions.add(new FrameworkVersion("1.0.0"));
		framework.setVersions(versions);
		mockMvc.perform(put("/edit").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
		.andExpect(jsonPath("$.id", is(4)))
		.andExpect(jsonPath("$.name", is("Random.js")))
		.andExpect(jsonPath("$.levelOfFame", is(200)))
		.andExpect(jsonPath("$.endOfDays", is(500)));
		
		mockMvc.perform(get("/versions/4")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$", hasSize(2)));
		
		mockMvc.perform(get("/4")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.id", is(4)))
		.andExpect(jsonPath("$.name", is("Random.js")))
		.andExpect(jsonPath("$.levelOfFame", is(200)))
		.andExpect(jsonPath("$.endOfDays", is(500)));
		
		mockMvc.perform(get("/5")).andExpect(status().isNotFound());
		
		mockMvc.perform(delete("/4")).andExpect(status().isNoContent());
		
		mockMvc.perform(get("/4")).andExpect(status().isNotFound());
		mockMvc.perform(get("/versions/4")).andExpect(status().isNotFound());
	} 
	
}
