package com.etnetera.hr.data;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity
@Table(name = "java_script_framework")
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Size(max = 30)
	@Column(nullable = false, length = 30)
	private String name;
	
	@OneToMany(mappedBy="framework")
	private Set<FrameworkVersion> versions;
	
	@Column(name = "end_of_days")
	private Date endOfDays;
	
	@Column(name = "level_of_fame")
	private int levelOfFame;

	public Set<FrameworkVersion> getVersions() {
		return versions;
	}

	public void setVersions(Set<FrameworkVersion> versions) {
		this.versions = versions;
	}

	public Date getEndOfDays() {
		return endOfDays;
	}

	public void setEndOfDays(Date endOfDays) {
		this.endOfDays = endOfDays;
	}

	public int getLevelOfFame() {
		return levelOfFame;
	}

	public void setLevelOfFame(int levelOfFame) {
		this.levelOfFame = levelOfFame;
	}

	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "JavaScriptFramework [id=" + id + ", name=" + name + "]";
	}

}
