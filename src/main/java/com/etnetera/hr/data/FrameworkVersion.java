package com.etnetera.hr.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "framework_version")
public class FrameworkVersion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String version;
	
	@ManyToOne
	@JoinColumn(name = "framework_id")
	@JsonIgnore
	@NotNull
	private JavaScriptFramework framework;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public JavaScriptFramework getFramework() {
		return framework;
	}

	public void setFramework(JavaScriptFramework framework) {
		this.framework = framework;
	}

	public FrameworkVersion() {
		
	}
	
	public FrameworkVersion(String version) {
		
	}
}
