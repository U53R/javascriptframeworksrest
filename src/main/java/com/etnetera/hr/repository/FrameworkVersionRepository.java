package com.etnetera.hr.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.etnetera.hr.data.FrameworkVersion;
import com.etnetera.hr.data.JavaScriptFramework;

public interface FrameworkVersionRepository extends CrudRepository<FrameworkVersion, Long> {
	
	public Set<FrameworkVersion> findByFramework(JavaScriptFramework framework);
	
	@Query(value = "SELECT v.version FROM framework_version v JOIN java_script_framework f ON v.framework_id = f.id WHERe f.id = :id",
			nativeQuery = true)
	public Set<String> getFrameworkVersions(@Param("id") Long id);
}
