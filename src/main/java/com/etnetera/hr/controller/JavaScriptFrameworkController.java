package com.etnetera.hr.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.etnetera.hr.data.FrameworkVersion;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.FrameworkVersionRepository;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.rest.Errors;
import com.etnetera.hr.rest.ValidationError;

/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
public class JavaScriptFrameworkController extends EtnRestController {

	private final JavaScriptFrameworkRepository repository;
	
	private final FrameworkVersionRepository versionRepository;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptFrameworkRepository repository, FrameworkVersionRepository versionRepository) {
		this.repository = repository;
		this.versionRepository = versionRepository;
	}

	@GetMapping("/frameworks")
	public Iterable<JavaScriptFramework> frameworks() {
		return repository.findAll();
	}
	
	@PostMapping("/add")
	public JavaScriptFramework create(@Valid @RequestBody JavaScriptFramework framework) {
		Set<FrameworkVersion> versions = framework.getVersions();
		framework.setVersions(null);
		framework = repository.save(framework);
		if (versions != null) {
			for (FrameworkVersion version : versions) {
				version.setFramework(framework);
				versionRepository.save(version);
			}
		}
		return framework;
	}
	
	@PutMapping("/edit")
	public JavaScriptFramework edit(@Valid @RequestBody JavaScriptFramework framework) {
		Set<FrameworkVersion> versions = framework.getVersions();
		framework.setVersions(null);
		framework = repository.save(framework);
		for (FrameworkVersion version : versionRepository.findByFramework(framework)) {
			versionRepository.delete(version);
		}
		if (versions != null) {
			for (FrameworkVersion version : versions) {
				version.setFramework(framework);
				versionRepository.save(version);
			}
		}
		return framework;
	}
	
	@GetMapping("/{id}")
	public JavaScriptFramework find(@PathVariable Long id, HttpServletResponse response) {
		Optional<JavaScriptFramework> framework = repository.findById(id);
		if (framework.isPresent()) {
			return framework.get();
		}
		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return null;
	}
	
	@DeleteMapping("/{id}")
	public List<JavaScriptFramework> delete(@PathVariable Long id, HttpServletResponse response) {
		Optional<JavaScriptFramework> framework = repository.findById(id);
		if (framework.isPresent()) {
			for (FrameworkVersion version : versionRepository.findByFramework(framework.get())) {
				versionRepository.delete(version);
			}
			repository.delete(framework.get());
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return new ArrayList<JavaScriptFramework>();
		}
		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return null;
	}
	
	@GetMapping("/versions/{id}")
	public Set<FrameworkVersion> getVersions(@PathVariable Long id, HttpServletResponse response) {
		Optional<JavaScriptFramework> framework = repository.findById(id);
		if (framework.isPresent()) {
			return versionRepository.findByFramework(framework.get());
		}
		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return null;
	}

}
